package in.ashish29agre.rabbiroidmq;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.QueueingConsumer;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeoutException;

public class MainActivity extends AppCompatActivity {

    private static final String EXCHANGE = "amq.fanout";
    private static final String CHAT = "chat";

    private ConnectionFactory connectionFactory;
    private PublisherThread publisherThread;

    private SubscriberThread subscriberThread;
    private BlockingDeque<String> queue;
    private MessageHandler messageHandler;
    private List<String> messages;
    private MessageAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        connectionFactory = new ConnectionFactory();
        try {
            connectionFactory.setAutomaticRecoveryEnabled(true);
            connectionFactory.setUri(BuildConfig.Uri);
            queue = new LinkedBlockingDeque<>();
            publisherThread = new PublisherThread(connectionFactory);
            publisherThread.start();
            subscriberThread = new SubscriberThread(connectionFactory);
            subscriberThread.start();
        } catch (URISyntaxException | NoSuchAlgorithmException | KeyManagementException e) {
            e.printStackTrace();
        }

        ImageView btnSend = (ImageView) findViewById(R.id.mq_btn_send);
        final EditText editText = (EditText) findViewById(R.id.mq_msg);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    queue.putLast(editText.getText().toString());
                    editText.setText("");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        messageHandler = new MessageHandler();
        messages = new ArrayList<>();
        adapter = new MessageAdapter(messages);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.msg_recy_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        publisherThread.interrupt();
        subscriberThread.interrupt();
    }

    private class PublisherThread extends Thread {

        private Connection connection;
        private Channel channel;

        public PublisherThread(ConnectionFactory connectionFactory) {
            if (connectionFactory == null) {
                throw new IllegalArgumentException("ConnectionFactory null");
            }
        }


        @Override
        public void run() {
            try {
                connection = connectionFactory.newConnection();
                channel = connection.createChannel();
                channel.confirmSelect();
            } catch (IOException | TimeoutException e) {
                e.printStackTrace();
                throw new IllegalStateException("Error: " + e.getMessage());
            }
            while (true) {
                String message = null;
                try {
                    message = queue.takeFirst();
                    channel.basicPublish(EXCHANGE, CHAT, null, message.getBytes());
                    channel.waitForConfirmsOrDie();
                } catch (InterruptedException | IOException e) {
                    if (message != null)
                        try {
                            queue.putFirst(message);
                        } catch (InterruptedException e1) {
                            e1.printStackTrace();
                        }
                    e.printStackTrace();
                    try {
                        Thread.sleep(5000);
                        start();
                    } catch (InterruptedException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        }

    }

    private class SubscriberThread extends Thread {

        private Channel channel;
        private Connection connection;

        public SubscriberThread(ConnectionFactory connectionFactory) {
            if (connectionFactory == null) {
                throw new IllegalArgumentException("ConnectionFactory null");
            }
        }


        @Override
        public void run() {
            try {
                connection = connectionFactory.newConnection();
                channel = connection.createChannel();
                channel.confirmSelect();
            } catch (IOException | TimeoutException e) {
                e.printStackTrace();
                throw new IllegalStateException("Error: " + e.getMessage());
            }
            try {
                channel.basicQos(1);
                AMQP.Queue.DeclareOk q = channel.queueDeclare();
                channel.queueBind(q.getQueue(), EXCHANGE, CHAT);
                QueueingConsumer consumer = new QueueingConsumer(channel);
                channel.basicConsume(q.getQueue(), true, consumer);

                while (true) {

                    QueueingConsumer.Delivery delivery = consumer.nextDelivery();
                    String message = new String(delivery.getBody());
                    Message msg = messageHandler.obtainMessage();
                    Bundle bundle = new Bundle();
                    bundle.putString("msg", message);
                    msg.setData(bundle);
                    messageHandler.sendMessage(msg);
                }
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    private class MessageHandler extends Handler {

        MessageHandler() {

        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String message = msg.getData().getString("msg");
            messages.add(message);
            adapter.notifyDataSetChanged();
        }
    }

    class MessageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private final List<String> messages;

        MessageAdapter(List<String> messages) {
            this.messages = messages;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new TextViewHolder(new TextView(parent.getContext()));
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            TextView textView = (TextView) holder.itemView;
            textView.setText(messages.get(position));
        }

        @Override
        public int getItemCount() {
            return messages.size();
        }

        class TextViewHolder extends RecyclerView.ViewHolder {

            public TextViewHolder(View itemView) {
                super(itemView);
            }
        }
    }
}
